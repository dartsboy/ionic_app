angular.module('app.controllers', ['ngCordova', 'ngConstellation'])
  
.controller('homeCtrl', ['$scope', '$rootScope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $stateParams) {
}])
   
.controller('joinAGameCtrl', ['$scope', '$rootScope', '$state','$stateParams', '$cordovaBarcodeScanner', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $state, $stateParams, $cordovaBarcodeScanner) {
 $scope.openScan = function(){
		document.addEventListener("deviceready", function () {
			$cordovaBarcodeScanner
		  	.scan()
		  	.then(function(barcodeData) {
                $rootScope.joinID = barcodeData.text;
                $state.go('connect');
		  	}, function(error) {
		    	$scope.error();
		  	});
		}, false);
	};
}])
   
.controller('connectCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'constellationConsumer',
function ($scope, $rootScope, $state, $stateParams, constellation) {
    function initConstellation(){
        gameInfo = $rootScope.joinID.split("-");
        constellation_url = gameInfo[0];//"http://81.66.10.255:8088";
        constellation_key = gameInfo[1];//"d97503f52c2289b4862db5c0438a08e86c0e1a41";
        constellation_clientName = "MyDartCompagnon";
        constellation.initializeClient(constellation_url, constellation_key, constellation_clientName);
        
        constellation.onConnectionStateChanged(function (change) {
            $scope.ConnectionToConstellationState = change.newState;
            if (change.newState === $.signalR.connectionState.connected) {
                //S'abonner au stateobject game
                constellation.registerStateObjectLink("*", "DartManager", "DartGame", "*", function (so) {
                     $scope.$apply(function() {
                         $scope.constellationGame = so.Value; 
                    });
                //Envoyer son identifiant
                constellation.sendMessage({Scope: 'Group', Args: ['DartsDashboard'] }, 'join', $rootScope.user.providerData[0]);
                });
                setTimeout(function (){
                    $scope.$apply(function(){
                        setGameState('connected');
                    });
                }, 1500);
            }
        });
        constellation.connect();
    }
    
    function setGameState(newState){
        $scope.gameState = newState;
    }
    
    function getImage(value){
        if(value == 1){
            return "/img/GoalCount/simple.png";
        }
        if(value == 2){
            return "/img/GoalCount/double.png";
        }
        if(value >= 3){
            return "/img/GoalCount/triple.png";
        }
    }
    
    function getShot(dart){
        if(dart.Type == 1){
            return dart.Value;
        }
        if(dart.Type == 2){
            return dart.Value + "x2";
        }
        if(dart.Type == 3){
            return dart.Value + "x3";
        }
        if(dart.Type == 0){
            return "Miss";
        }
    }
    
    $scope.gameState = 'init'; //initialized
    $scope.getImage = getImage;
    $scope.getShot = getShot;
    initConstellation();

}])


.controller('myFriendsCtrl', ['$scope', '$rootScope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $stateParams) {
    $scope.friends = {
        0 : {
            name : "Maxime Buguel",
            level : "12",
            pp : "/img/friends/maxime.png"
            },
        1 : {
            name : "Charles Cartiaux",
            level : "15",
            pp : "/img/friends/charles.png"
        },
        2 : {
            name : "Thomas Douillard",
            level : "11",
            pp : "/img/friends/thomas.png"
            }
    }
}])
   
.controller('myStatsCtrl', ['$scope', '$rootScope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $stateParams) {
    $scope.stats = {
        0 : {
            name : "Wins",
            imgSrc : "/img/stats/trophy.png",
            value : "25%",
            sum : "1/5"
            },
        1 : {
            name : "Accuracy",
            imgSrc : "/img/stats/accuracy.png",
            value : "78%",
            sum : "300 shots"
        },
        2 : {
            name : "Place",
            imgSrc : "/img/stats/place.png",
            value : "3 locations",
            sum : "5 Games"
            }
    }

}])
   
.controller('myHistoryCtrl', ['$scope', '$rootScope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $stateParams) {
    $scope.history = {
        0 : {
            date : "Lundi 14 Novembre, 14h50",
            type : "Cut Throat Game",
            classement : "2nd",
            score : 38
            },
        1 : {
            date : "Dimanche 13 Novembre, 18h12",
            type : "Cut Throat Game",
            classement : "1st",
            score : 18
        },
        2 : {
            date : "Dimanche 13 Novembre, 17h35",
            type : "Cut Throat Game",
            classement : "3rd",
            score : 80
            },
        3 : {
            date : "Samedi 12 Novembre, 18h40",
            type : "301 Game",
            classement : "2nd",
            score : 55
            },
        4 : {
            date : "Jeudi 10 novembre, 15h58",
            type : "Cut Throat Game",
            classement : "4th",
            score : 125
            }
    }

}])
   
.controller('myTrophiesCtrl', ['$scope', '$rootScope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $stateParams) {

    $scope.success = {
        unlocked : {
            0 : {
                title : "Bourreau",
                details : "Infliger 200 points dans une partie"
            },
            1 : {
                title : "Le compte est bon",
                details : "Remporter une partie de type 301"
            },
            2 : {
                title : "En plein dans le mille",
                details : "Placer une fléchette dans le D-Bull"
            },
            3 : {
                title : "Edinson Cavani",
                details : "3 miss dans un round"
            },
            4 : {
                title : "Sniper",
                details : "Finir une partie avec 90% de précision"
            }
        },
        
        locked : {
            0 : {
                title : "Experimenté",
                details : "Disputer 100 parties"
            },
            1 : {
                title : "Social Game",
                details : "Inviter des amis à disputer une partie"
            },
            2 : {
                title : "Envoie un message à ta copine...",
                details : "Réaliser 3 triples consécutifs"
            },
            3 : {
                title : "Joueur précoce",
                details : "Finir un cut throat en moins de 10 rounds"
            }
        }
    }

}])
   
.controller('settingsCtrl', ['$scope', '$rootScope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $stateParams) {


}])
   
.controller('loginCtrl', ['$scope', '$rootScope', '$state', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $state, $stateParams) {

    function signInWithFacebook(){
		var provider = new firebase.auth.FacebookAuthProvider();
        provider.addScope('user_friends');
        firebase.auth().signInWithPopup(provider).then(function(result) {
          // This gives you a Facebook Access Token. You can use it to access the Facebook API.
          $rootScope.fbAuth = result;
          $rootScope.user = result.user;
            console.log(result.user.providerData[0]);
          $state.go('home');
        }).catch(function(error) {
          console.log('Error during the connection');
            console.log(error);
        });  
	}
    
	$scope.signInWithFacebook = signInWithFacebook;

}])
 