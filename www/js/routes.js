angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

  .state('home', {
    url: '/home',
    templateUrl: 'templates/home.html',
    controller: 'homeCtrl'
  })

  .state('joinAGame', {
    url: '/join',
    templateUrl: 'templates/joinAGame.html',
    controller: 'joinAGameCtrl'
  })

  .state('connect', {
    url: '/connect',
    templateUrl: 'templates/connection.html',
    controller: 'connectCtrl'
  })
  
  
  .state('myFriends', {
    url: '/friends',
    templateUrl: 'templates/myFriends.html',
    controller: 'myFriendsCtrl'
  })

  .state('myStats', {
    url: '/stats',
    templateUrl: 'templates/myStats.html',
    controller: 'myStatsCtrl'
  })

  .state('myHistory', {
    url: '/history',
    templateUrl: 'templates/myHistory.html',
    controller: 'myHistoryCtrl'
  })

  .state('myTrophies', {
    url: '/trophies',
    templateUrl: 'templates/myTrophies.html',
    controller: 'myTrophiesCtrl'
  })

  .state('settings', {
    url: '/settings',
    templateUrl: 'templates/settings.html',
    controller: 'settingsCtrl'
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

$urlRouterProvider.otherwise('/login')

  

});